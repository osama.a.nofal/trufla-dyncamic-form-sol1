import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { INPUTS_TEMPLATE } from './inputs-example';

@Component({
  selector: 'trufla-fast-add-input',
  templateUrl: './fast-add-input.component.html',
  styleUrls: ['./fast-add-input.component.scss']
})
export class FastAddInputComponent implements OnInit {

  @Output()
  onAdd = new EventEmitter();

  modalOpened: boolean = false;
  isHelpModal;

  titleModal;
  helpContent;

  inputType = "TEXTBOX";

  currentField: FormlyFieldConfig;

  constructor() { }

  ngOnInit(): void {
  }

  addInput() {
    this._preOpenModal();
    this.titleModal = `add new ${this.inputType} input`;
    this.isHelpModal = false;

  }

  showHelp() {
    this._preOpenModal();
    this.titleModal = `${this.inputType} schema example`;
    this.isHelpModal = true;
    this.helpContent = JSON.stringify(this.currentField || {}, null, 2);
  }

  private _preOpenModal(): void {
    this.currentField = INPUTS_TEMPLATE[this.inputType]
    this.modalOpened = true;

  }
  onAddInput() {
    if (this.currentField) {
      this.onAdd.emit(this.currentField);
      this.onModalClose();
    } else {
      alert("not avalid json")
    }
  }
  onJsonChage(json): void {
    try {
      this.currentField = JSON.parse(json);
    } catch (e) {

    }
  }
  onModalClose() {
    this.modalOpened = false;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FastAddInputComponent } from './fast-add-input.component';

describe('FastAddInputComponent', () => {
  let component: FastAddInputComponent;
  let fixture: ComponentFixture<FastAddInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FastAddInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FastAddInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

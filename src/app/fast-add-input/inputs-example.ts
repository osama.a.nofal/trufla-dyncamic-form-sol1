export const INPUTS_TEMPLATE = {

    "TEXTBOX": {
        "key": "keyText", // DB field
        "type": "input",
        "templateOptions": {
            "label": "Enter field value",
            "placeholder": "Enter field value",
        }
    },


    "DROPDOWN": {
        "key": "keyDrop", // DB field
        "type": "select",//****
        "templateOptions": {
            "label": "Enter field value",
            "placeholder": "Enter field value",
            "options": [
                { "label": "Label1", "value":"DValue1" },
                { "label": "Label2", "value":"DValue2" },
                { "label": "Label3", "value":"DValue3" },
            ]
        }
    },

    "RADIOBUTTON": {
        "key": "keyDrop", // DB field
        "type": "select",//****
        "templateOptions": {
            "label": "Enter field value",
            "options": [
                { "label": "Radio1", "value":"RValue1" },
                { "label": "Radio2", "value":"RValue2" },
                { "label": "Radio3", "value":"RValue3" },
            ]
        }
    },


    "CHECKBOX": {
        "key": "keyCheck", // DB field
        "type": "checkbox",//****
        "templateOptions": {
            "label": "Enter field value",
        }
    },


    "SUBMIT": {
        "key": "keySubmit", // DB field
        "type": "submit",//****
        "templateOptions": {
            "label": "Form Submit",
        }
    },

    "CANCEL": {
        "key": "keySubmit", // DB field
        "type": "cancel",//****
        "templateOptions": {
            "label": "Cancel",
        }
    }

}

export const SUPPORTED_INPUTS = ['input','select','checkbox','radio','submit','cancel']
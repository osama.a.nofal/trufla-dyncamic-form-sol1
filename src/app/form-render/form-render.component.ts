import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { INPUTS_TEMPLATE } from './../fast-add-input/inputs-example';

@Component({
  selector: 'trufla-form-render',
  templateUrl: './form-render.component.html',
  styleUrls: ['./form-render.component.scss']
})
export class FormRenderComponent implements OnInit {

  _form = new FormGroup({});
  _options:FormlyFormOptions = {};
  _formFields: FormlyFieldConfig[] = [];

  @Input()
  set fields(fields: FormlyFieldConfig[]){
    this._formFields = JSON.parse(JSON.stringify(fields)); // simple deep copy    
  }

  @Input()
  model: any = {};

  @Output()
  onSubmit: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }


  _onFormSubmit() {
    if (this._form.valid)
      this.onSubmit.emit(this.model);
    else {
      alert("form is invalid")
    }
  }
  _onFormCancel(){
    this._options.resetModel();
  }
}

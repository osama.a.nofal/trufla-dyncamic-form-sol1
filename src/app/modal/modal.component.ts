import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'trufla-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Output()
  close = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onClose() {
    this.close.emit('close')
  }



}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'trufla-json-validator',
  templateUrl: './json-validator.component.html',
  styleUrls: ['./json-validator.component.scss']
})
export class JsonValidatorComponent implements OnInit {

  @Input()
  label
  @Input()
  set json(input: any) {
    if(typeof input === "string"){
      this._textAreaValue = input;
    }else if(typeof input ==="object" && input){
      this._formatJson(input);
    }
  };

  @Output()
  jsonChange: EventEmitter<string> = new EventEmitter();

  jsonError: string;
  _textAreaValue:string = "";
  _dirty;

  constructor() { }

  ngOnInit(): void {
  }

  textChanged(event: Event): void {
    const textArea = event.target as HTMLTextAreaElement
    const value = (textArea.value || "").trim();
    this._dirty = true;

    this.jsonError = null;

    if (value && this._validateJson(value)) {
      this.jsonChange.emit(value);
    }
    if(!value){
      this.jsonChange.emit(null);
    }


  }

  private _validateJson(jsonString: string): boolean {
    let isValid = true;
    try {
      const object = JSON.parse(jsonString);
      this._formatJson(object);
    } catch (ex) {
      console.error(ex);
      this.jsonError = ex;
      isValid = false;
    }
    return isValid;
  }

  private _formatJson(object) {
    const value = JSON.stringify(object, null, 2);
    this._textAreaValue = value || "";
  }
}

// import modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from "@angular/forms";
import { FormlyModule } from "@ngx-formly/core";
import { FormlyBootstrapModule } from "@ngx-formly/bootstrap";

// import Components
import { ChallengeDemoComponent } from './challenge-demo/challenge-demo.component';
import { JsonValidatorComponent } from './json-validator/json-validator.component';
import { FormDesignerComponent } from './form-designer/form-designer.component';
import { FormRenderComponent } from './form-render/form-render.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ButtonFieldComponent } from './formly-custom/button-field/button-field.component';
import { FastAddInputComponent } from './fast-add-input/fast-add-input.component';
import { ModalComponent } from './modal/modal.component';


@NgModule({
  declarations: [
    ChallengeDemoComponent,
    JsonValidatorComponent,
    FormDesignerComponent,
    FormRenderComponent,
    NavBarComponent,
    FastAddInputComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      // custom inputes
      types: [
        {
          name: 'Label',
          wrappers: ['form-field']
        },
        {
          name: 'submit',
          component: ButtonFieldComponent,
          wrappers: [],
          defaultOptions: {
            templateOptions: {
              btnType: 'default btn-primary',
              type: 'submit',
            },
          },
        },
        {
          name: 'cancel',
          component: ButtonFieldComponent,
          wrappers: [],
          defaultOptions: {
            templateOptions: {
              btnType: 'default btn-danger',
              type: 'cancel',
            },
          },
        },
      ],
      validationMessages: [{
        name: "required",
        message: "this field is required"
      }]
    }),
    FormlyBootstrapModule
  ],
  providers: [],
  bootstrap: [ChallengeDemoComponent]
})
export class TruflaModule { }

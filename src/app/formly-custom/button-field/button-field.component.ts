
import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
  selector: 'trufla-button-field',
  templateUrl: './button-field.component.html',
  styleUrls: ['./button-field.component.scss']
})
export class ButtonFieldComponent extends FieldType implements OnInit {

  ngOnInit(): void {

    
    this.formControl.valueChanges.subscribe(()=> delete this.model[this.key as string])
  }

  onClick(event:Event) {
   
    if (this.to.type === "submit") {
      
    } else if (this.to.type === "cancel") {
      this.options.resetModel();
      this.options.updateInitialValue()
      event.preventDefault()
      event.stopPropagation();
    }
  }
}


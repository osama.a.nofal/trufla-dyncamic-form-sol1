import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'trufla-challenge-demo',
  templateUrl: './challenge-demo.component.html',
  styleUrls: ['./challenge-demo.component.scss']
})
export class ChallengeDemoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

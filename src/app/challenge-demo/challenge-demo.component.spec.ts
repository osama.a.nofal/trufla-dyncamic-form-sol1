import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChallengeDemoComponent } from './challenge-demo.component';

describe('ChallengeDemoComponent', () => {
  let component: ChallengeDemoComponent;
  let fixture: ComponentFixture<ChallengeDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChallengeDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengeDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

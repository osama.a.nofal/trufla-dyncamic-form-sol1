import { Component, OnInit } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { INPUTS_TEMPLATE, SUPPORTED_INPUTS } from './../fast-add-input/inputs-example';

const INIT_FIELD = {
  key: 'name',
  type: 'input',
  templateOptions: {
    label: 'Enter your name',
    placeholder: 'Enter your name'
  }
}

@Component({
  selector: 'trufla-form-designer',
  templateUrl: './form-designer.component.html',
  styleUrls: ['./form-designer.component.scss']
})
export class FormDesignerComponent implements OnInit {


  formFields: FormlyFieldConfig[] = [
    INIT_FIELD
  ];

  model = {};

  constructor() { }

  ngOnInit(): void {
  }

  onModelChange(model: any) {

  }

  onFastAddInput(field: FormlyFieldConfig): void {
    this.formFields = [...this.formFields, field]
  }
  onSubmit(model): void {
    alert("form submited :"+JSON.stringify(model,null,2))
  }
  formJsonChanged(json: string) {
    try {
      const json2Object = JSON.parse(json);
      this.formFields = json2Object || [];
      this.model = {};
    } catch (e) {
      // shouldn't happen coz json is emitted after validated with true
      this.formFields = []
      alert("something went wrong")
    }

  }

  getUnsupportedInputs(){
    return this.formFields.filter((field)=> SUPPORTED_INPUTS.indexOf(field.type) == -1);

  }
}
